import os
import pandas as pd
from subprocess import Popen, PIPE
from torquepi import settings


def read_df_from_command(df_command):
	p = Popen(df_command, stdout=PIPE)
	df_output_file = p.stdout
	return read_df_output(df_output_file)

def read_df_output(df_output_file):
	#/dev/sdb1             3.3T  951G  2.2T  31% /scratch
	disk_dict = {}
	for line in df_output_file:
		line = line.strip()
		for disk in settings.SERVER_DISKS:
			if line.endswith(disk):
				(_, _, _, avail, use_perc, mounted_on) = line.split()
				disk_dict[mounted_on] = {"avail" : avail,
										 "use_perc" : use_perc}


	df = pd.DataFrame.from_dict(disk_dict, orient='index')
	return df




if __name__ == '__main__':
	df_output_file_name = os.path.join(settings.BASE_DIR, "server_stats", "test_data", "sample_qstats_merge_output.txt")
	if False:
		with open(df_output_file_name) as df_output_file:
			data = read_df_output(df_output_file)
			print data
	else:
		data = read_df_from_command(settings.POLL_SERVER_DF_COMMAND)
		print data
