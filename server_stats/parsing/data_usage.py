'''
Created on 18Oct.,2016

@author: dlawrence
'''

from datetime import datetime
import re
from subprocess import check_output

from torquepi import settings

def get_remote_file_last_modified(file_name):
    stat_command = settings.REMOTE_COMMAND_BASE + ["stat", "--format=%Y", file_name]
    stat_output = check_output(stat_command)
    return datetime.fromtimestamp(int(stat_output))
    
def get_remote_time():
    date_command = settings.REMOTE_COMMAND_BASE + ["date", "+%s"]
    date_output = check_output(date_command)
    return datetime.fromtimestamp(int(date_output))


def get_time_since_last_update(file_name):
    now = get_remote_time()
    last_modified_date = get_remote_file_last_modified(file_name)
    diff = now - last_modified_date
    minutes = diff.seconds / 60
    hours = minutes / 60

    if diff.days:
        return "%d days" % diff.days
    elif hours:
        return "%d hours" % hours
    elif minutes:
        return "%d minutes" % minutes
    else:
        return "%s seconds" % diff.seconds
    
        

def get_subdirectory_usage():
    pattern = re.compile("^(\d+)G\s+(.*)$")
    subdirectory_usage_output = check_output(settings.USAGE_SUBDIRECTORY_COMMAND).split('\n')

    subdirectory_usage = []
    for line in subdirectory_usage_output:
        m = pattern.match(line)
        if m:
            gigs = int(m.group(1))
            directory = m.group(2)
            directory = directory.replace(settings.STRIP_SUBDIR_PREFIX, "")
            subdirectory_usage.append((directory, gigs))
    
    subdirectory_usage = list(sorted(subdirectory_usage, key=lambda x : x[1]))
    return subdirectory_usage
