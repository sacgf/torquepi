from StringIO import StringIO
import os
import re
from subprocess import check_output

import numpy as np
import pandas as pd
from torquepi import settings
from torquepi.settings import POLL_SERVER_QSTAT_MERGE_COMMAND

QSTATS_NO_JOBS = 'No jobs on the queue'

def get_qstat_lines():
	''' returns array of qstat lines or None if no jobs '''
	qstat_text = check_output(POLL_SERVER_QSTAT_MERGE_COMMAND)
	qstat_lines = qstat_text.split("\n")
	if qstat_lines[0] == QSTATS_NO_JOBS:
		qstat = []
	else:
		qstat = qstat_lines
	return qstat, qstat_text


def read_qstats_from_command(qstats_command):
	qstats_file = check_output(qstats_command).split('\n')
	return read_qstats(qstats_file)

def get_colspecs_from_line_whitespace(line):
	colspecs = []
	start = 0
	
	for i, c in enumerate(line):
		if c.isspace():
			if start is not None:
				colspecs.append((start, i))
			start = None
		else:
			if start is None:
				start = i

	if start is not None:
		colspecs.append((start, i))
	
	return colspecs

def qstats_df(qstats_file_lines):
	if type(qstats_file_lines) != list:
		msg = "Parameter qstats_file_lines must be a list"
		raise ValueError(msg)
	
	line_headers = qstats_file_lines[3]
	colspecs = get_colspecs_from_line_whitespace(line_headers)
	names = ["Job ID", "Job Name", "Owner", "Server", "Session ID", "Req. nodes", "Req. ppn", "Req. mem", "Req. vmem", "Status", "Used cput", "Used walltime"] 

	rest_of_file = '\n'.join(qstats_file_lines[4:])
	f = StringIO(rest_of_file)

	df = pd.read_fwf(f, names=names, colspecs=colspecs)	
	df = df.dropna(how='all')
	mask = df[names[0]] == "Total"
	first_total_row = df.loc[mask].index.min()
	df = df.ix[:first_total_row-1] # Remove footer after Total
	
	if not settings.QSTAT_SHOW_COMPLETED_JOBS:
		not_complete_jobs_mask = df["Status"] != 'C'
		df = df[not_complete_jobs_mask]
	
	return df
	

def read_qstats(qstats_file):
	PARAMS = {"float_regex" : settings.FLOAT_REGEX}
	QSTATS_ROW_LOOKUP_REGEX = {
		"running_total_mem" : re.compile(r"Total memory requested in running jobs \(GB\):\s+(%(float_regex)s)" % PARAMS),
		"running_total_vmem" : re.compile(r"Total virtual memory requested in running jobs \(GB\):\s+(%(float_regex)s)" % PARAMS),
		"running_total_cores" : re.compile(r"Total processors requested in running jobs:\s+(\d+)"),
		"running_jobs" : re.compile(r"Number of running jobs \( R \):\s+(\d+)"),
		"queued_jobs" : re.compile(r"Number of running jobs \( Q \):\s+(\d+)"),
		"hold_jobs" : re.compile(r"Number of running jobs \( H \):\s+(\d+)"),
	}

	qstats = {"total_cores" : settings.SERVER_TOTAL_CORES,
			  "total_mem" : settings.SERVER_TOTAL_MEM,
	}
	for k in QSTATS_ROW_LOOKUP_REGEX: # Default everything to 0
		qstats[k] = 0

	for line in qstats_file:
		for (key, regex) in QSTATS_ROW_LOOKUP_REGEX.items():
			m = regex.match(line)
			if m:
				value = m.group(1)
				if '.' in value:
					value = float(value)
				else:
					value = int(value)
				qstats[key] = value 

	return qstats


def qstats_summary_by_user(df):
	summary_df = pd.DataFrame()
	if not df.empty:
		users = {}
		for (user, status), sub_df in df.groupby(["Owner", "Status"]):
			data = users.get(user, {"R" : 0, "Q" : 0, "H" : 0, "E" : 0, "C" : 0}) # Has to be there for sort
			data.update({status : len(sub_df)})
			users[user] = data
	
		print "users: ", users
		summary_df = pd.DataFrame.from_dict(users, orient='index')
		sort_columns = ["E", "R"]
		summary_df = summary_df.replace(np.NaN, 0).sort(columns=sort_columns, ascending=False)
	return summary_df
	
if __name__ == '__main__':
	qstats_file_name = os.path.join(settings.BASE_DIR, "server_stats", "test_data", "sample_qstats_merge_output.txt")
	if False:
		with open(qstats_file_name) as qstats_file:
			stats = read_qstats(qstats_file)
			print stats
	else:
		qstats_command = ["cat", qstats_file_name]
		print "qstats_command : %s" % qstats_command
		qstats_lines = check_output(qstats_command).split('\n')
#		stats = read_qstats_from_command(qstats_command).split('\n')
		df = qstats_df(qstats_lines)
		for user, (r, q) in qstats_summary_by_user(df).iterrows():
			print "user=%s, (r=%s, q=%s)" % (user, r, q)
