'''
Created on 15/07/2016

@author: dlawrence
'''

import operator
import os
import re
from subprocess import check_output

from torquepi import settings


VALID_IMAGE_EXTENSIONS = {".png", ".jpg", ".gif"}

def get_user_images():
    files = []
    for f in os.listdir(settings.USER_SCRATCH_IMAGE_DIR):
        full_path = os.path.join(settings.USER_SCRATCH_IMAGE_DIR, f)
        if os.path.isfile(full_path):
            scratch_path = os.path.join(settings.USER_SCRATCH_IMAGE_STATIC_PATH, f)
            files.append(scratch_path)

    user_images = {}
    for f in files:
        username, extension = os.path.splitext(os.path.basename(f))
        if extension in VALID_IMAGE_EXTENSIONS:
            user_images[username] = f
    return user_images


def read_user_scratch_usage_from_command(user_scratch_command, user_list):
    scratch_usage = check_output(settings.USER_SCRATCH_COMMAND).split('\n')
    return read_user_scratch_usage(scratch_usage, user_list)

def read_user_scratch_usage(scratch_output_file, user_list):
    pattern = re.compile(r"^(\d+)G\s+.*\/([^\/]+)$")

    scratch_per_user = {}
    for line in scratch_output_file:
        m = pattern.match(line)
        if m:
            gb_usage = int(m.group(1))
            user = m.group(2)

            if user in user_list:
                scratch_per_user[user] = gb_usage

    return scratch_per_user 
        

def get_user_scratch_usage(command=settings.USER_SCRATCH_COMMAND):
    ''' Returns a list of ('user', GB usage, IMAGE) in sorted order '''
    
    user_images = get_user_images()
    scratch_per_user = read_user_scratch_usage_from_command(command, user_images)
    
    users_by_usage = sorted(scratch_per_user.items(), key=operator.itemgetter(1))
    user_scratch_usage_images = []
    for (username, usage) in users_by_usage:
        data = (username, usage, user_images[username])
        user_scratch_usage_images.append(data)

    return user_scratch_usage_images

if __name__ == '__main__':
    fake_scratch_data = os.path.join(settings.BASE_DIR, "server_stats", "test_data", "sample_scratch.txt")
    data = get_user_scratch_usage(command=['cat', fake_scratch_data])
    print data