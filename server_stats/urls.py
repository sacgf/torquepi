from django.conf.urls import patterns, url
from server_stats import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^view_page_number/(?P<page_number>\d+)$', views.view_page_number, name='view_page_number'),
)