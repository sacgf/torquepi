from django.shortcuts import render_to_response
from django.template.context import RequestContext
import json
import random
import numpy as np

from server_stats.parsing.data_usage import get_subdirectory_usage, \
    get_time_since_last_update
from server_stats.parsing.df import read_df_from_command
from server_stats.parsing.qstat import read_qstats, qstats_df, \
    qstats_summary_by_user, get_qstat_lines
from server_stats.parsing.user_scratch_usage import get_user_scratch_usage
from torquepi import settings
from torquepi.settings import POLL_SERVER_DF_COMMAND


def server_queue_text(request, context):
    return render_to_response('server_stats/server_queue_text.html', RequestContext(request, context))


def server_disk(request, context):
    df = read_df_from_command(POLL_SERVER_DF_COMMAND)
    context["df"] = df.transpose().to_json()
    return render_to_response('server_stats/server_disk.html', RequestContext(request, context))

def other_page(request, context):
    return render_to_response('server_stats/other.html', RequestContext(request, context))


def scratch_by_user(request, context):
    user_scratch_usage = get_user_scratch_usage()
    context["user_scratch_usage"] = json.dumps(user_scratch_usage)
    context["last_updated"] = get_time_since_last_update(settings.CRON_JOB_SCRATCH_USAGE_REPORT)
    return render_to_response('server_stats/user_scratch_usage.html', RequestContext(request, context))

def server_subdirectory_usage(request, context):
    GRAPH_TYPES = ["bar", "pie"]

    last_updated = get_time_since_last_update(settings.CRON_JOB_SUBDIRECTORY_USAGE_REPORT)    
    subdirectory_usage = get_subdirectory_usage()
    context["graph_title"] = "%s Usage (Last Updated %s ago)" % (settings.STRIP_SUBDIR_PREFIX, last_updated)
    context["subdirectory_usage"] = json.dumps(subdirectory_usage)
    context["graph_type"] = random.choice(GRAPH_TYPES)
    
    return render_to_response('server_stats/server_subdirectory_usage.html', RequestContext(request, context))


def index(request):
    return view_page_number(request, 0)

def view_page_number(request, page_number):    
    ''' Returns a random page '''

    page_number = int(page_number)
    refresh = request.GET.get("refresh", "True") == "True"
    if refresh:
        refresh_seconds = settings.BROWSER_REFRESH_SECS
    else:
        refresh_seconds = None # Disable

    context = {"next_page_number" : page_number + 1,
               "refresh_seconds" : refresh_seconds,}

    sub_category_func = server_queue_text
    try:
        PAGES = {"server_queue_text" : server_queue_text,
                 "server_disk" : server_disk,
                 "server_subdirectory_usage" : server_subdirectory_usage,
                 "scratch_by_user" : scratch_by_user,
                 "other" : other_page,  
        }
        
    
        qstats_lines, qstat_text = get_qstat_lines()      
        qstats = read_qstats(qstats_lines)
        page = request.GET.get("page")
        if page:
            sub_category_func = PAGES[page]
        else:
            PAGE_TREE = [
                [server_disk, scratch_by_user, server_subdirectory_usage]#, other_page],
            ]
            if qstats["running_jobs"] > 0: # Only show this one if running jobs
                PAGE_TREE.append([server_queue_text])
    
            toplevel_cycle_number = page_number / len(PAGE_TREE)
            toplevel_category = PAGE_TREE[page_number % len(PAGE_TREE)]
            sub_category_func = toplevel_category[toplevel_cycle_number % len(toplevel_category)]
    
        # Base cpu etc graphs
        qstats_user = {}
        if qstats_lines:
            df = qstats_df(qstats_lines)
            context['qstats_df_html'] = df.replace(np.NaN, '').to_html(index=False)
            qstats_user = qstats_summary_by_user(df)
    
        context["qstat_text"] = qstat_text
        context["qstats_json"] = json.dumps(qstats)
        context["qstats_user"] = qstats_user
    except Exception as e:
        context["exception"] = e
    
    return sub_category_func(request, context)

