# TorquePi #

![DSC_0045_640x480.JPG](https://bitbucket.org/repo/xB7qge/images/38779160-DSC_0045_640x480.JPG)

RaspberryPi info-panel for Bioinformatics room. [SetupRaspberryPi](https://bitbucket.org/sacgf/torquepi/wiki/SetupRaspberryPi)

http://10.32.88.167:8000

### Server Monitor ###

* Server CPU and memory usage
* Disk Usage

### How does it work? ###

* Pi runs Django server as a service
* Pi boots into X, running Chromium in full screen kiosk mode
* The server runs ssh commands on SACGF to check server state
* This is then used to build Javascript graphs on the web page
* Page automatically reloads every 5 seconds, choosing different stats to display